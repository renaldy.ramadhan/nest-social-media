FROM node:18-alpine as base

ARG database_connection
ARG database_host
ARG database_port
ARG database_name
ARG database_user
ARG database_password
ARG database_sync
ARG jwt_secret

FROM base AS deps

RUN apk update && apk add --no-cache make gcc g++ python3

WORKDIR /app

COPY package.json yarn.lock* package-lock.json* pnpm-lock.yaml* ./
RUN yarn global add pnpm && pnpm i --frozen-lockfile

FROM base AS builder

WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY .env.example .env.production
COPY . .

RUN npm run build

FROM base AS runner
WORKDIR /app

ENV NODE_ENV production
ENV DB_CONNECTION ${database_connection}
ENV DB_HOST ${database_host}
ENV DB_PORT ${database_port}
ENV DB_USER ${database_user}
ENV DB_PASSWORD ${database_password}
ENV DB_DATABASE ${database_name}
ENV DB_SYNCRONIZE ${database_sync}
ENV JWT_SECRET ${jwt_secret}

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nestjs

COPY --from=builder --chown=nestjs:nodejs /app/package*.json ./
COPY --from=builder --chown=nestjs:nodejs /app/node_modules/ ./node_modules/
COPY --from=builder --chown=nestjs:nodejs /app/dist/ ./dist/
COPY --from=builder --chown=nestjs:nodejs /app/.env.production ./

USER nestjs

EXPOSE 3000

ENV HOSTNAME "0.0.0.0"

CMD [ "node", "dist/main" ]