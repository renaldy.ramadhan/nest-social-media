module.exports = {
    extends: ['@commitlint/config-conventional'],
    rules: {
        "type-enum" : [2,"always",['feat','chore','fix','refactor']],
        "type-case" : [2,"always",['lower-case']],
        "subject-empty" : [2,"never"]
    }
}