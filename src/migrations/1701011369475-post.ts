import { MigrationInterface, QueryRunner } from "typeorm";

export class Post1701011369475 implements MigrationInterface {
    name = 'Post1701011369475'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "posts" ALTER COLUMN "updatedAt" SET DEFAULT now()`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "posts" ALTER COLUMN "updatedAt" DROP DEFAULT`);
    }

}
