import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpException, HttpStatus, ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors) => {
        const result = errors.map((error) => {
          const messageValue = Object.values(error.constraints);
          const stringMessage = messageValue.filter((value) => typeof value);
          return {
            property: error.property,
            message: stringMessage,
          };
        });
        return new HttpException(
          {
            message: 'Invalid Request',
            result,
          },
          HttpStatus.BAD_REQUEST,
        );
      },
    }),
  );
  await app.listen(3000);
}
bootstrap();
