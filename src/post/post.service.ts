import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PostEntity } from './entity/post-entity';
import { Repository } from 'typeorm';
import { CreatePostDTO } from './dto/create-post.dto';
import { AuthService } from 'src/auth/auth.service';
import { UpdatePostDTO } from './dto/update-post.dto';

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(PostEntity) private postRepository: Repository<PostEntity>,
    private authService: AuthService,
  ) {}

  async getPost(): Promise<PostEntity[]> {
    return this.postRepository
      .createQueryBuilder('posts')
      .innerJoinAndSelect('posts.user', 'user')
      .orderBy('posts.createdAt', 'DESC')
      .getMany();
  }

  async createPost(postDTO: CreatePostDTO, username: string): Promise<PostEntity> {
    const user = await this.authService.getUser(username);
    return this.postRepository.save({
      ...postDTO,
      user,
    });
  }

  async updatePost(postDTO: UpdatePostDTO, postId: number): Promise<number> {
    const { affected: result } = await this.postRepository.update(postId, { ...postDTO });
    return result;
  }

  async deletePost(postId: number, username: string): Promise<number> {
    const user = await this.authService.getUser(username);
    const { affected: result } = await this.postRepository.delete({ id: postId, user: user });
    console.log(result);
    return result;
  }
}
