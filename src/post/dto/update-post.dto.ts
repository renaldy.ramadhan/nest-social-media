import { IsNotEmpty } from 'class-validator';

export class UpdatePostDTO {
  @IsNotEmpty({ message: 'Field is required' })
  body: string;
}
