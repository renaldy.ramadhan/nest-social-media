import { IsNotEmpty } from 'class-validator';

export class CreatePostDTO {
  @IsNotEmpty({
    message: 'Field is required',
  })
  body: string;
}
