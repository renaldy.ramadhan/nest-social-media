import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
// import { Request } from 'express';
import { CreatePostDTO } from './dto/create-post.dto';
import { UpdatePostDTO } from './dto/update-post.dto';
import { PostEntity } from './entity/post-entity';
import { PostService } from './post.service';
import { AuthGuard } from 'src/guard/auth.guard';

@Controller('post')
export class PostController {
  constructor(private postService: PostService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(AuthGuard)
  @Get()
  async findAll(): Promise<ResponseAPI> {
    const posts: PostEntity[] = await this.postService.getPost();
    return {
      message: 'Post fetched!',
      statusCode: HttpStatus.OK,
      result: posts,
    };
  }

  @UseGuards(AuthGuard)
  @UseInterceptors(ClassSerializerInterceptor)
  @Post('new')
  async create(@Body() body: CreatePostDTO, @Req() request: Request): Promise<ResponseAPI> {
    const post: PostEntity = await this.postService.createPost(body, request['user'].username);
    return {
      message: 'Post Created',
      statusCode: HttpStatus.CREATED,
      result: post,
    };
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(AuthGuard)
  @Get('/:id')
  async findOne(@Param('id') id: string): Promise<string> {
    return `find one post ${id}`;
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(AuthGuard)
  @Put(':id')
  async update(@Param('id') id: number, @Body() request: UpdatePostDTO): Promise<ResponseAPI> {
    const result: number = await this.postService.updatePost(request, id);
    if (!result) {
      return {
        message: 'Post not found!',
        statusCode: HttpStatus.NOT_FOUND,
      };
    }

    return {
      message: 'Row updated',
      statusCode: HttpStatus.OK,
      result: request,
    };
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(AuthGuard)
  @Delete(':id')
  async delete(@Param('id') id: number, @Req() request: Request) {
    const result: number = await this.postService.deletePost(id, request['user'].username);
    if (!result) {
      return {
        message: 'Post not found!',
        statusCode: HttpStatus.NOT_FOUND,
      };
    }

    return {
      message: 'Post deleted',
      statusCode: HttpStatus.OK,
    };
  }
}
