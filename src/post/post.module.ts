import { Module } from '@nestjs/common';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostEntity } from './entity/post-entity';
import { AuthService } from 'src/auth/auth.service';
import { UserEntity } from 'src/auth/entity/user-entity';

@Module({
  controllers: [PostController],
  providers: [PostService, AuthService],
  imports: [TypeOrmModule.forFeature([PostEntity, UserEntity])],
})
export class PostModule {}
