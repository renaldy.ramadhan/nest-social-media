import { config as dotenvConfig } from 'dotenv';

dotenvConfig({ path: `.env.${process.env.NODE_ENV}` });

export const jwtConstant = {
  secret: process.env.JWT_SECRET,
};
