interface ResponseAPI {
  message: string;
  statusCode: integer;
  result?: any;
}
