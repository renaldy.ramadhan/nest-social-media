import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './entity/user-entity';
import { Repository } from 'typeorm';
import { RegisterDTO } from './dto/register-dto';
import * as bcrypt from 'bcrypt';
import { LoginDto } from './dto/login-dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  private constOrRound: number = 10;
  constructor(
    @InjectRepository(UserEntity) private userRepository: Repository<UserEntity>,
    private jwtService: JwtService,
  ) {}

  async createUser(userDTO: RegisterDTO): Promise<UserEntity> {
    const hashed = await bcrypt.hash(userDTO.password, this.constOrRound);
    return this.userRepository.save({ ...userDTO, password: hashed });
  }

  async authUser(userDTO: LoginDto): Promise<any> {
    const user: UserEntity = await this.userRepository.findOneBy({ email: userDTO.email });
    if (!user) {
      throw new UnauthorizedException();
    }

    const isMatch = await bcrypt.compare(userDTO.password, user.password);
    if (!isMatch) {
      throw new UnauthorizedException();
    }

    const payload = { sub: user.id, username: user.username };
    return {
      access_token: await this.jwtService.signAsync(payload),
      user: {
        fullname: user.fullname,
        username: user.username,
        email: user.email,
      },
    };
  }

  async userProfile(username: string): Promise<UserEntity> {
    return this.userRepository.findOneBy({ username: username });
  }

  async getUser(username: string): Promise<UserEntity> {
    const user: UserEntity = await this.userRepository.findOneBy({ username: username });
    return user;
  }
}
