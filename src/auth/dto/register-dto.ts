import { IsEmail, IsNotEmpty, MinLength } from 'class-validator';
export class RegisterDTO {
  @IsNotEmpty({ message: 'Field is required!' })
  fullname: string;

  @IsNotEmpty({ message: 'Field is required!' })
  username: string;

  @IsNotEmpty({ message: 'Field is required!' })
  @IsEmail({}, { message: 'Email format is invalid!' })
  email: string;

  @IsNotEmpty({ message: 'Field is required!' })
  @MinLength(8, { message: 'Passwod is too short' })
  password: string;
}
