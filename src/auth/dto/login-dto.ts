import { IsNotEmpty } from 'class-validator';

export class LoginDto {
  @IsNotEmpty({ message: 'Field is required!' })
  email: string;

  @IsNotEmpty({ message: 'Field is required!' })
  password: string;
}
