import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Request,
  Res,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { LoginDto } from './dto/login-dto';
import { RegisterDTO } from './dto/register-dto';
import { AuthService } from './auth.service';
import { UserEntity } from './entity/user-entity';
import { AuthGuard } from '../guard/auth.guard';
import { Response } from 'express';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  async login(
    @Body() request: LoginDto,
    @Res() response: Response,
  ): Promise<Response<ResponseAPI>> {
    const authUser = await this.authService.authUser(request);

    response.cookie('x-token', authUser.access_token, { httpOnly: true, domain: 'localhost' });
    return response.json({
      message: 'User Auth Success',
      statusCode: HttpStatus.OK,
      result: authUser,
    });
  }

  @Post('register')
  @HttpCode(201)
  @UseInterceptors(ClassSerializerInterceptor)
  async register(@Body() request: RegisterDTO): Promise<ResponseAPI> {
    const user: UserEntity = await this.authService.createUser(request);
    return {
      message: 'User Created',
      statusCode: HttpStatus.CREATED,
      result: {
        fullname: user.fullname,
        username: user.username,
        email: user.email,
        createdAt: user.createdAt,
      },
    };
  }

  @UseGuards(AuthGuard)
  @UseInterceptors(ClassSerializerInterceptor)
  @Get('profile')
  async getProfile(@Request() request: Request): Promise<ResponseAPI> {
    const user: UserEntity = await this.authService.userProfile(request['user'].username);
    return {
      message: 'User Fetched',
      statusCode: HttpStatus.OK,
      result: user,
    };
  }
}
